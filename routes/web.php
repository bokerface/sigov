<?php

use App\Http\Livewire\About;
use App\Http\Livewire\Auth;
use App\Http\Livewire\Home;
use App\Http\Livewire\Inbox;
use App\Http\Livewire\Profile;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', Auth::class)->name('login');

Route::middleware('isLoggedIn')->group(function () {
    Route::get('home', Home::class)->name('home');
    Route::get('about', About::class)->name('about');
    Route::get('inbox', Inbox::class)->name('inbox');
    Route::get('profile', Profile::class)->name('profile');
});
