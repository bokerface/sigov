<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class Auth extends Component
{
    public $email, $password;

    public function render()
    {
        return view('livewire.auth');
    }

    // private function resetInputFields()
    // {
    //     $this->email = '';
    //     $this->password = '';
    // }

    public function login()
    {
        $data = [
            'username' => $this->email,
            'password' => $this->password,
        ];


        $params = http_build_query($data);
        $email = $this->email;
        $body = array('http' =>
        array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => $params
        ));
        $context = stream_context_create($body);
        $link = file_get_contents('https://sso.umy.ac.id/api/Authentication/Login', false, $context);
        $json = json_decode($link);
        $ceknum = $json->{'code'};

        if ($ceknum == 0) {
            $mahasiswa = DB::connection('sqlsrv')
                ->table('V_Mahasiswa')
                ->where('EMAIL', '=', $email)
                ->first();

            if ($mahasiswa) {
                $user_data = [
                    "user_id" => $mahasiswa->STUDENTID,
                    "fullname" => $mahasiswa->FULLNAME,
                    "email" => $mahasiswa->EMAIL,
                    "role" => 3,
                    "isLoggedIn" => true
                ];

                Session::put("user_data", $user_data);
                // dd(session('user_data'));

                return redirect()->to(route('home'));
            }
        }

        session()->flash('error', 'email and password are wrong.');
    }
}
