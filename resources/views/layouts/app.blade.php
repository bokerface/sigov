<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <title>Azures BootStrap</title>
    <!-- <link rel="stylesheet" type="text/css" href="styles/bootstrap.css"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('styles/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('styles/customsd.css') }}">
    <!-- <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;display=swap" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('fonts/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/css/icomoon.css') }}">
    <link rel="manifest" href="{{ asset('_manifest.json') }}"
        data-pwa-version="set_in_manifest_and_pwa_js">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('app/icons/icon-192x192.png') }}">

    <style>
        .header.sigov {
            background: linear-gradient(to bottom right, #361928 0%, #141118 100%);
        }

        .header-logo-app a.header-title.mcustomsd {
            margin-left: 30px !important;
        }

    </style>

    @stack('inline-styles')

    <livewire:scripts />
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js"
        data-turbolinks-eval="false" data-turbo-eval="false"></script>
    <livewire:styles />
</head>

<body class="theme-light bgsd-white" data-highlight="highlight-pink2">

    <div id="preloader">
        <div class="spinner-border color-highlight" role="status"></div>
    </div>

    <div id="page">

        <!-- header and footer bar go here-->
        <div class="header sigov header-fixed header-auto-show header-logo-app">
            <a href="index.html" class="header-title mcustomsd text-white">SIGOV</a>

        </div>
        <div id="footer-bar" class="footer-bar-5 rounded-0">

            <a href="{{ route('inbox') }}"><i class="icon-mail"></i></a>

            <a href="{{ route('home') }}" class="active-nav"><i class="icon-home"></i></a>

            <a href="{{ route('profile') }}" data-menu="menu-settings"><i class="icon-user"></i></a>
        </div>

        <div class="page-content">
            {{ $slot }}
            <div id="menu-share" class="menu menu-box-bottom menu-box-detached rounded-m"
                data-menu-load="menu-share.html" data-menu-height="420" data-menu-effect="menu-over">
            </div>

            <div id="menu-highlights" class="menu menu-box-bottom menu-box-detached rounded-m"
                data-menu-load="menu-colors.html" data-menu-height="510" data-menu-effect="menu-over">
            </div>

            <div id="menu-main" class="menu menu-box-right menu-box-detached rounded-m" data-menu-width="260"
                data-menu-load="menu-main.html" data-menu-active="nav-welcome" data-menu-effect="menu-over">
            </div>

            <!-- Be sure this is on your main visiting page, for example, the index.html page-->
            <!-- Install Prompt for Android -->
            <div id="menu-install-pwa-android" class="menu menu-box-bottom menu-box-detached rounded-l"
                data-menu-height="350" data-menu-effect="menu-parallax">
                <div class="boxed-text-l mt-4">
                    <img class="rounded-l mb-3" src="app/icons/icon-128x128.png" alt="img" width="90">
                    <h4 class="mt-3">SIGOV on your Home Screen</h4>
                    <p>
                        Install SIGOV on your home screen, and access it just like a regular app. It really is that
                        simple!
                    </p>
                    <a href="#"
                        class="pwa-install btn btn-s rounded-s shadow-l text-uppercase font-900 bg-highlight mb-2">Add
                        to
                        Home Screen</a><br>
                    <a href="#"
                        class="pwa-dismiss close-menu color-gray2-light text-uppercase font-900 opacity-60 font-10">Maybe
                        later</a>
                    <div class="clear"></div>
                </div>
            </div>

            <!-- Install instructions for iOS -->
            <div id="menu-install-pwa-ios" class="menu menu-box-bottom menu-box-detached rounded-l"
                data-menu-height="320" data-menu-effect="menu-parallax">
                <div class="boxed-text-xl mt-4">
                    <img class="rounded-l mb-3" src="app/icons/icon-128x128.png" alt="img" width="90">
                    <h4 class="mt-3">SIGOV on your Home Screen</h4>
                    <p class="mb-0 pb-3">
                        Install SIGOV on your home screen, and access it just like a regular app.
                        Open your Safari menu and tap "Add to Home Screen".
                    </p>
                    <div class="clear"></div>
                    <a href="#"
                        class="pwa-dismiss close-menu color-highlight font-800 opacity-80 text-center text-uppercase">
                        Maybe later
                    </a>
                    <br>
                    <i class="fa-ios-arrow fa fa-caret-down font-40"></i>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="{{ asset('scripts/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('scripts/custom.js') }}"></script>
</body>

</html>
